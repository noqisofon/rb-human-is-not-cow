require 'spec_helper'

require_relative '../lib/human'
require_relative '../lib/cow'

describe Cow do

  it '牛はにんげんではない' do
    expect( described_class ).to_not equal Human
  end

  it '牛はにんげんではない 2' do
    a_cow = described_class.new

    expect( a_cow ).to_not be_a Human
  end

end
