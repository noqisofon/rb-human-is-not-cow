require 'spec_helper'

require_relative '../lib/human'
require_relative '../lib/cow'

describe Human do

  it 'にんげんは牛ではない' do
    expect( described_class ).to_not equal Cow
  end

  it 'にんげんは牛ではない 2' do
    a_human = described_class.new

    expect( a_human ).to_not be_a Cow
  end

end
